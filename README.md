Patch for RJW and addon suite that fills in some missing textures, generally male, hulking, and fat variants of wearables.

# Currently Patched: #
RimJobWorld - Extension (rimworld.ekss.rjwex)
* Textures for chastity cage facing other directions